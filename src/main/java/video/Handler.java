/**
 * 
 */
package video;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Handler extends Thread {

    static Logger log = LoggerFactory.getLogger(Handler.class);
    /**
     * 
     */
    final Server server;
    final Socket client;
    final LinkedBlockingQueue<byte[]> queue = new LinkedBlockingQueue<byte[]>();

    public Handler(Server server, Socket client) {
        this.server = server;
        this.client = client;
    }

    public boolean add(byte[] packet) {
        if (queue.size() > 8) {
            queue.remove(0);
        }
        if (queue.offer(packet)) {
            return true;
        } else {
            log.warn("Dropping packet for client {}", client.getRemoteSocketAddress());
            return false;
        }
    }

    public void run() {

        try {
            OutputStream out = client.getOutputStream();
            DataOutputStream dos = new DataOutputStream(out);

            while (true) {
                try {

                    byte[] packet = queue.take();
                    dos.write(packet);
                    // dos.write(packet.length);
                    // dos.write(packet);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        } catch (Exception err) {
            err.printStackTrace();
        } finally {
            server.clientFinished(this);
        }
    }
}