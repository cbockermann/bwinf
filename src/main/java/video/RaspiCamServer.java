/**
 * 
 */
package video;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.JpegStream;

/**
 * @author chris
 *
 */
public class RaspiCamServer extends Server {

    static Logger log = LoggerFactory.getLogger(RaspiCamServer.class);

    long packets = 0L;
    long bytes = 0L;

    public RaspiCamServer(int port) throws Exception {
        super(port);
    }

    public void run() {
        while (true) {
            try {
                Socket client = server.accept();
                Handler handler = new Handler(this, client);
                handler.start();

                synchronized (handlers) {
                    handlers.add(handler);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clientFinished(Handler handler) {
        synchronized (handlers) {
            handlers.remove(handler);
        }
    }

    public void add(byte[] packet) {
        int added = 0;
        int failed = 0;
        bytes += packet.length;
        synchronized (handlers) {
            for (Handler h : handlers) {
                if (h.add(packet)) {
                    added++;
                } else {
                    failed++;
                }
            }
        }
        log.debug("Broadcasted packet to {} clients, {} clients failed.", added, failed);
        packets++;
        if (packets % 1000 == 0) {
            log.info("{} packets broadcasted, {} kbytes", packets, bytes / 1024);
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        log.info("Starting TCP server on port 9000");
        RaspiCamServer server = new RaspiCamServer(9000);

        Socket client = server.server.accept();
        OutputStream out = client.getOutputStream();
        log.info("Received client-connect from {}", client.getRemoteSocketAddress());
        // server.start();

        int every = Integer.parseInt(System.getProperty("every", "1"));

        int id = 0;
        log.info("Reading RAW mjpeg data from {}", args[0]);
        JpegStream stream = new JpegStream(new FileInputStream(args[0]), 1024 * 1024, 16 * 1024 * 1024, true);
        byte[] buf = stream.readNextChunk();
        while (buf != null) {
            // server.add(buf);
            if (id % every == 0) {
                out.write(buf);
            }
            id++;
            buf = stream.readNextChunk();
        }

    }
}
