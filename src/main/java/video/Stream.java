/**
 * 
 */
package video;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;
import stream.io.JpegStream;
import stream.io.LittleEndianDataInputStream;
import stream.io.SourceURL;

/**
 * @author chris
 *
 */
public class Stream extends AbstractStream {

    static Logger log = LoggerFactory.getLogger(Stream.class);
    String host;
    int port;

    Socket socket;
    DataInputStream dis;

    public Stream(SourceURL url) {
        super(url);
        host = url.getHost();
        port = url.getPort();
    }

    /**
     * @see stream.io.AbstractStream#init()
     */
    @Override
    public void init() throws Exception {
        super.init();

        log.info("Connecting to server at {}:{}", host, port);
        socket = new Socket(host, port);
        dis = new DataInputStream(socket.getInputStream());
    }

    /**
     * @see stream.io.AbstractStream#readNext()
     */
    @Override
    public Data readNext() throws Exception {

        int size = dis.readInt();
        byte[] buf = new byte[size];

        dis.readFully(buf);

        Data item = DataFactory.create();
        item.put("data", buf);
        return item;
    }

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket("192.168.128.120", 9000);

        InputStream in = socket.getInputStream();
        InputStream dis = in;
        dis = new LittleEndianDataInputStream(in);
        int packets = 0;
        byte[] buf = new byte[32000];
        int read = dis.read(buf, 0, buf.length);
        while (read > 0) {

            packets++;

            if (packets % 100 == 0) {
                log.info("{} packets received...", packets);
            }

            int idx = indexOf(JpegStream.JPEG_SOI, buf, read);
            if (idx >= 0) {
                log.info("Found JPEG_SOI at {}", idx);
            }

            idx = indexOf(JpegStream.JPEG_EOI, buf, read);
            if (idx >= 0) {
                log.info("Found JPEG_EOI at {}", idx);
            }

            read = dis.read(buf, 0, buf.length);
        }

        dis.close();
        socket.close();
    }

    public static int indexOf(byte[] sig, byte[] buffer, int limit) {

        int max = Math.min(buffer.length - sig.length, limit);

        for (int i = 0; i < max; i++) {
            int found = 0;
            for (int s = 0; s < sig.length; s++) {
                if (buffer[i + s] == sig[s]) {
                    found++;
                } else {
                    break;
                }
            }
            if (found == sig.length) {
                return i;
            }
        }

        return -1;
    }
}