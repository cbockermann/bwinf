/**
 * 
 */
package video;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.JpegStream;

/**
 * @author chris
 *
 */
public class Server extends Thread {

    static Logger log = LoggerFactory.getLogger(Server.class);
    ServerSocket server;
    final List<Handler> handlers = new ArrayList<Handler>();

    long packets = 0L;
    long bytes = 0L;

    public Server(int port) throws Exception {
        server = new ServerSocket(port);
    }

    public void run() {
        while (true) {
            try {
                Socket client = server.accept();
                Handler handler = new Handler(this, client);
                handler.start();

                synchronized (handlers) {
                    handlers.add(handler);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clientFinished(Handler handler) {
        synchronized (handlers) {
            handlers.remove(handler);
        }
    }

    public void add(byte[] packet) {
        int added = 0;
        int failed = 0;
        bytes += packet.length;
        synchronized (handlers) {
            for (Handler h : handlers) {
                if (h.add(packet)) {
                    added++;
                } else {
                    failed++;
                }
            }
        }
        log.debug("Broadcasted packet to {} clients, {} clients failed.", added, failed);
        packets++;
        if (packets % 1000 == 0) {
            log.info("{} packets broadcasted, {} kbytes", packets, bytes / 1024);
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        Server server = new Server(9000);
        server.start();

        if (args.length == 0) {
            log.info("Reading frames from local network...");
            runNetworkMode(server);
        } else {
            log.info("Reading frames from {}", args[0]);
            long start = System.currentTimeMillis();

            InputStream in = new FileInputStream(new File(args[0]));
            JpegStream video = new JpegStream(in, 1024 * 1024, 16 * 1024 * 1024, true);

            Integer id = 0;
            byte[] chunk = video.readNextChunk();
            while (chunk != null) {
                log.info("Chunk {} is: {}", id, chunk);
                id++;
                log.debug("Broadcasting chunk {} ({} bytes)", id, chunk.length);
                server.add(chunk);

                if (id % 100 == 0) {
                    long end = System.currentTimeMillis();
                    Double sec = (end - start) / 1000.0d;
                    log.info("Feed read with {} kb/s", id.doubleValue() / sec);
                }

                chunk = video.readNextChunk();
            }
            long end = System.currentTimeMillis();
            Double sec = (end - start) / 1000.0d;
            log.info("Feed read with {} fps", id.doubleValue() / sec);
        }
    }

    public static void runNetworkMode(Server server) throws Exception {

        int bs = Integer.parseInt(System.getProperty("bs", "8192"));

        ServerSocket receiver = new ServerSocket(9001, 50, InetAddress.getByName("127.0.0.1"));
        log.info("Waiting for local input at  {}", receiver.getLocalSocketAddress());

        while (true) {
            try {
                Socket feed = receiver.accept();
                log.info("Accepted feed from {}", feed.getRemoteSocketAddress());

                long start = System.currentTimeMillis();

                InputStream in = feed.getInputStream();
                // FileInputStream fis = new FileInputStream(new
                // File("/Volumes/RamDisk/kapseln.mjpeg"));
                // JpegStream video = new JpegStream(in, 1024 * 1024, 16 * 1024
                // * 1024, true);

                Integer id = 0;
                Long bytes = 0L;
                byte[] chunk = new byte[bs];
                int read = in.read(chunk, 0, chunk.length);
                while (chunk != null && read > 0) {
                    id++;
                    byte[] out = new byte[read];
                    System.arraycopy(chunk, 0, out, 0, read);
                    log.debug("Broadcasting chunk {} ({} bytes)", id, out.length);
                    server.add(out);
                    bytes += read;

                    if (id % 1000 == 0) {
                        long end = System.currentTimeMillis();
                        Double sec = (end - start) / 1000.0d;
                        log.info("Feed read with {} kb/sec", (bytes.doubleValue() / 1024.0) / sec);
                    }

                    read = in.read(chunk, 0, chunk.length);
                    // chunk = video.readNextChunk();
                }
                long end = System.currentTimeMillis();
                Double sec = (end - start) / 1000.0d;
                log.info("Feed read with {} kb/sec", (bytes.doubleValue() / 1024.0) / sec);

            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
        receiver.close();
    }
}
