package streams.tutorial.astro.analyze;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;
import streams.tutorial.astro.Constants;
import streams.tutorial.astro.PrintOutput;

/**
 * Created by alexey on 22.09.14.
 */
public class LineClassification extends AbstractImageProcessor {

    final static Logger log = LoggerFactory.getLogger(LineClassification.class);

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    @Override
    public Data process(Data item, ImageRGB img) {
        List<Integer[]> points = new ArrayList<Integer[]>();
        Integer threshold = (Integer) item.get("threshold");
        threshold = threshold==null ? 120 : threshold;
        double oneThird = 1 / 3.0;

        for (int y = 0; y < img.height; y++) {

            final int widY = img.width * y;
            for (int x = 0; x < img.width; x++) {

                int pixel = img.pixels[widY + x];

                int red = (pixel >> 16) & 0xFF;
                int green = (pixel >> 8) & 0xFF;
                int blue = pixel & 0xFF;

                int grey = (int) ((red + green + blue) * oneThird);

                if (grey > threshold) {
                    Integer[] point = new Integer[2];
                    point[0] = x;
                    point[1] = y;
                    points.add(point);
                }
            }
        }

        if (points.size() > 2) {
            double m = 0;
            double b = 0;
            double mOrthogonal = 0;
            // detect the direction of the line
            Integer[] first = points.get(0);
            Integer[] last = points.get(points.size() - 1);
            double mT = ((double) first[1] - last[1]) / ((double) first[0] - last[0]);
            if (mT > 0) {
                // from left down to right up
                int deltaY = -img.height;
                int deltaX = img.width;
                m = deltaX == 0 ? 0 : (double) deltaY / deltaX;
                b = img.height;
                mOrthogonal = m == 0 ? 0 : -1 / m;
            } else if (mT < 0) {
                // from right down to left up
                int deltaY = img.height;
                int deltaX = img.width;
                m = deltaX == 0 ? 0 : (double) deltaY / deltaX;
                b = 0;
                mOrthogonal = m == 0 ? 0 : -1 / m;
            } else if (mT == 0) {
                // from left to right (its a horizontal line)
                b = img.height;
            }

            double distanceSum = 0;
            double[] median = new double[points.size()];
            int i = 0;
            for (Integer[] point : points) {
                double bOrthogonal = -point[0] * mOrthogonal + point[1];
                double x = m == mOrthogonal ? 0 : (bOrthogonal - b) / (m - mOrthogonal);
                double y = m * x + b;
                median[i] = Math.sqrt(Math.pow(point[0] - x, 2) + Math.pow(point[1] - y, 2));
                distanceSum += median[i++];
            }
            distanceSum /= points.size();

            Arrays.sort(median);

            double result;
            switch (Constants.LINE_DETECT_METHOD){
                case Constants.AVERAGE_SUM_METHOD:
                    result = distanceSum;
                    break;
                case Constants.MEDIAN_METHOD:
                    result = median[points.size() / 2];
                    break;
                case Constants.VARIANCE:
                    double variance = 0;
                    for (i = points.size() - 1; i >= 0; i--){
                        variance += Math.pow(median[i] - distanceSum, 2);
                    }
                    variance /= (points.size()-1);
                    result = variance;
                    break;
                default:
                    result = distanceSum;
            }
            String output = (String) item.get(PrintOutput.PRINT_OUTPUT);
            output = (output != null ? output : "")
                    + "\nFeature: " + result + "\n";

            item.put("line_classification_result", result);

            boolean eventDetected = result < Constants.LINE_THRESHOLD;
            item.put("worm_detected", !eventDetected);

            if (item.containsKey("worm")) {
                Integer worm = (Integer) item.get("worm");

                if (eventDetected) {
                    output += "Event detected that is "
                            + (worm == 0 ? "an event" : "a worm")
                            + " in real.\n";
                } else {
                    output += "Worm detected that is "
                            + (worm == 0 ? "an event" : "a worm")
                            + " in real.\n";
                }
            }else{
                output += "Event detected.\n";
            }
            item.put(PrintOutput.PRINT_OUTPUT, output);
            log.info(output);
        }

        return item;
    }
}
