package streams.tutorial.astro;

import stream.AbstractProcessor;
import stream.Data;

/**
 * Created by alexey on 22.09.14.
 */
public class PrintOutput extends AbstractProcessor {

    public static String PRINT_OUTPUT = "print_output";

    String tag = PRINT_OUTPUT;

    public void setTag(String tag){
        this.tag = tag;
    }

    public Data process(Data input) {
//        String output = (String) input.get(tag);
        return input;
    }
}
