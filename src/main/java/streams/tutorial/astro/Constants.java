package streams.tutorial.astro;


import java.io.File;

/**
 * Created by alexey on 08.09.14.
 */
public class Constants {

    public static String
            ASTRO_DETECTOR_PATH = "/AstroDetector/",
            IMAGES_TEMP_PATH = "ImagesTemp";

    public final static String
            PROCESS_RESPONSE = "PROCESS_RESPONSE",
            TAG_CAMERA_FRAGMENT = "CAMERA_FRAGMENT",
            TAG = "BACKGROUND_PROCESS",
            RESPONSE = "RESPONSE",
            PROCESS_NAME = "PROCESS_NAME",
            CAMERA_LOAD_TITLE = "Loading camera preview...",
            CAMERA_LOAD_MESSAGE = "Please wait.";

    public final static int
            CAMERA_LOAD_TIME = 2000,
            WAITING_FOR_PHOTO_DONE = 100,
            WAITING_FOR_FILES = 500,
            AVERAGE_SUM_METHOD = 0,
            MEDIAN_METHOD = 1,
            VARIANCE = 2;

    public static boolean
            PREVIEW_RESTARTED = true,
            LOGGING = true,
            USE_THREADS = false;

    public static int PICTURE_SIZE = 0;
    public static int JPEG_COMPRESSION = 50;
    public static double LINE_THRESHOLD = 10;
    public static int LINE_DETECT_METHOD = Constants.AVERAGE_SUM_METHOD;

    public static String getImagesTempPath(){
    	return "/tmp/";
    }

    /***
     *
     * @param folder file (folder) to which the whole path will be created
     */
    public static boolean createFolder(File folder){
        if (!folder.exists()){
            if (folder.mkdirs()){
                System.out.println("Folder " + folder + " created.");
                return true;
            }
        }
        return false;
    }

    /***
     *
     * @param folder path to a folder which should be emptied
     */
    public static void removeFolderContent(File folder){
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    file.delete();
                }
            }
        }else{
            System.out.println("Error while removing content of a folder: \n"+folder+"\nList of files is null.");
        }
    }
}
