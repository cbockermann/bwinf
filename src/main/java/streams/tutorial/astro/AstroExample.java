/**
 * 
 */
package streams.tutorial.astro;

import java.net.URL;

/**
 * @author Christian Bockermann
 * 
 */
public class AstroExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		// fetch the XML url from the classpath
		//
		URL url = AstroExample.class
				.getResource("/astro/data-analysis.xml");

		// start the XML using the streams-runtime
		//
		stream.run.main(url);
	}
}
