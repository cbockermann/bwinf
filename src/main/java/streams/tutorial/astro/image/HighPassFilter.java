package streams.tutorial.astro.image;

import stream.Data;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

/**
 * Created by Matuschek on 01.09.2014.
 */
public class HighPassFilter extends AbstractImageProcessor {

    double cutoff = 0.3;
    double slope = 0.5;
    String output = AbstractImageProcessor.DEFAULT_IMAGE_KEY;

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void setCutoff(double cutoff) {
        if (cutoff > 1.0)
            cutoff = 1.0;
        if (cutoff < 0.0)
            cutoff = 0.0;
        this.cutoff = cutoff;
    }

    public void setSlope(double slope) {
        if (slope <= 0)
            slope = 0.01;
        this.slope = slope;
    }

    // currently unused
    boolean greyScale = true;
    boolean softKnee = false;

    private static final short RED = 0, GREEN = 1, BLUE = 2;

    @Override
    public Data process(Data input, ImageRGB img) {
        final double threshold = cutoff * 255;
        final double divideSlope = 1 / slope;

        final int stop = img.width * img.height;
        int[] rgb = new int[3];
        final double oneThird = 1.0 / 3;
        for (int i = stop - 1; i >= 0; i--) {
            int pixel = img.pixels[i];

            rgb[RED] = (pixel >> 16) & 0xFF;
            rgb[GREEN] = (pixel >> 8) & 0xFF;
            rgb[BLUE] = pixel & 0xFF;

            int grey = (int) ((rgb[RED] + rgb[GREEN] + rgb[BLUE]) * oneThird);

            if (grey < threshold) {
                int cut = (int) ((threshold - grey) * divideSlope);

                short j = 0;
                for (int col : rgb) {
                    col -= cut;
                    if (col < 0) col = 0;
                    if (col > 255) col = 255;
                    rgb[j++] = col;
                }

                img.pixels[i] = 255 << 24 & 0xFF000000 + (rgb[RED] << 16) & 0xFF0000
                        + (rgb[GREEN] << 8) & 0x00FF00 + (rgb[BLUE]) & 0xFF;
            }
        }
        input.put(output, new ImageRGB(img.width, img.height, img.pixels));
        return input;
    }
}
