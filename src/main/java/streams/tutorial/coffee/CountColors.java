/**
 * 
 */
package streams.tutorial.coffee;

import java.io.Serializable;

import stream.Data;
import stream.Processor;
import stream.data.Statistics;
import stream.statistics.StatisticsService;

/**
 * @author chris
 * 
 */
public class CountColors implements Processor, StatisticsService {

	String label = "@prediction";
	final Statistics counts = new Statistics();

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		counts.clear();
	}

	/**
	 * @see stream.statistics.StatisticsService#getStatistics()
	 */
	@Override
	public Statistics getStatistics() {
		return new Statistics(counts);
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		Serializable color = input.get(label);
		if (color != null) {
			counts.add(color.toString(), 1.0);
		}
		return input;
	}
}
