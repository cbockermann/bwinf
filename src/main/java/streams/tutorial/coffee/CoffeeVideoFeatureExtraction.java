/**
 * 
 */
package streams.tutorial.coffee;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class CoffeeVideoFeatureExtraction {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = CoffeeVideoFeatureExtraction.class
				.getResource("/chapter6/coffee-video-feature-extraction.xml");
		stream.run.main(url);
	}

}
