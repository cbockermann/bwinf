/**
 * 
 */
package streams.tutorial.coffee;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;
import stream.image.ImageRGB;

/**
 * @author chris
 * 
 */
public class WritePNG implements Processor {

	static Logger log = LoggerFactory.getLogger(WritePNG.class);

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer id = new Integer(input.get("@id") + "");
		if (id > 190 && id < 230) {
			ImageRGB img = (ImageRGB) input.get("frame:image");
			if (img != null) {
				File file = new File(
						"tex-src/chapter6-vista/figures/coffee/screenshot-"
								+ id + ".png");
				log.info("Writing image to file '{}'", file);
				try {
					BufferedImage bi = img.createImage();
					ImageIO.write(bi, "PNG", file);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return input;
	}
}