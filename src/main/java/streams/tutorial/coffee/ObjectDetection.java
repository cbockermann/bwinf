/**
 * 
 */
package streams.tutorial.coffee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.data.DataFactory;
import stream.io.Sink;

/**
 * @author chris
 * 
 */
public class ObjectDetection extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(ObjectDetection.class);

	public final static int red = 0;
	public final static int green = 1;
	public final static int blue = 2;

	final double[] min = new double[] { 255.0, 255.0, 255.0 };

	double threshold = 96.0;

	boolean belowThreshold = false;

	Double currentRed = null;
	Double currentGreen = null;
	Double currentBlue = null;

	Sink[] output;
	Data last = null;

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Double r = (Double) input.get("frame:red:average");
		Double g = (Double) input.get("frame:green:average");
		Double b = (Double) input.get("frame:blue:average");

		if (r > threshold && g > threshold && b > threshold) {
			//
			// we are ABOVE threshold, reset all minima !!
			//
			if (belowThreshold) {
				//
				// switching state!
				//
				// output the current mimimum
				if (last != null) {
					Data item = DataFactory.copy(last);
					item.put("object:red:min", min[red]);
					item.put("object:green:min", min[green]);
					item.put("object:blue:min", min[blue]);
					emit(item);
				}
				Data detected = last;
				// and reset everything:
				belowThreshold = false;
				min[red] = 255.0;
				min[green] = 255.0;
				min[blue] = 255.0;
				last = null;
				return detected;
			}
		} else {
			belowThreshold = true;

			boolean newMin = false;
			if (min[red] > r) {
				min[red] = r;
				newMin = true;
			}

			if (min[green] > g) {
				min[green] = g;
				newMin = true;
			}

			if (min[blue] > b) {
				min[blue] = b;
				newMin = true;
			}

			if (newMin) {
				last = input.createCopy();
			}
		}

		return null;
	}

	protected void emit(Data item) {
		if (output == null) {
			return;
		}

		for (Sink sink : output) {
			try {
				sink.write(item.createCopy());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the output
	 */
	public Sink[] getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(Sink[] output) {
		this.output = output;
	}

	/**
	 * @return the threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
}