/**
 * 
 */
package streams.tutorial.coffee;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class ReadingCoffeeVideo {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = ReadingCoffeeVideo.class
				.getResource("/chapter6/coffee-reading-video.xml");
		stream.run.main(url);
	}

}
