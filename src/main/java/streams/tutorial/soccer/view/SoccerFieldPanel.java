/**
 * 
 */
package streams.tutorial.soccer.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.text.DecimalFormat;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides a panel for drawing a soccer field and several position
 * objects, which have been provided by the update method.
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
public class SoccerFieldPanel extends JPanel {

	/** The unique class ID */
	private static final long serialVersionUID = -7149041781268662313L;
	static Logger log = LoggerFactory.getLogger(SoccerFieldPanel.class);

	final static int LAST_HITS = 100;
	final static int LAST_SHOTS = 5;
	final static Color GREEN = new Color(171, 194, 174);
	final double HALF_WIDTH = 52477.0d;
	final double HALF_HEIGHT = 33941.0d;

	String time = null;
	Long firstUpdate = null;
	Long count = 0L;
	final int border = 40;
	// final Map<Integer, double[]> positions = new ConcurrentHashMap<Integer,
	// double[]>();
	double[][] positions = new double[130][2];
	double[][] players = new double[24][2];
	int nearest = -1;
	int[] sensorRole = new int[130];

	int width = 0;
	int height = 0;
	int lastHit = -1;
	CopyOnWriteArrayList<double[]> hits = new CopyOnWriteArrayList<double[]>();
	int lastShot = -1;
	CopyOnWriteArrayList<double[]> shots = new CopyOnWriteArrayList<double[]>();
	final DecimalFormat fmt = new DecimalFormat("0.00");

	public void updateTime(String str) {
		time = str;
		if (time == null && firstUpdate != null) {
			Double dur = (System.currentTimeMillis() - firstUpdate) / 1000.0;
			time = "update #" + count + ", " + (count / dur) + " evts/sec";
		}

		for (int i = 0; i < sensorRole.length; i++) {
			sensorRole[i] = -1;
		}
	}

	/**
	 * @see javax.swing.JComponent#print(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		width = this.getWidth();
		height = this.getHeight();

		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// fill the panel with a green background
		//
		g.setColor(GREEN);
		g.fillRect(0, 0, width, height);

		// String t = time + ",  updates: #" + this.count;
		// if (firstUpdate != null) {
		// Double dur = (System.currentTimeMillis() - firstUpdate) / 1000.0;
		// t = t + ", data rate: " + fmt.format(count / dur) + " evts/sec";
		// }
		//
		// // draw the current game-time (if it has been set)
		// //
		// if (t != null) {
		// Font f = g.getFont().deriveFont(10.0f);
		// g.setFont(f);
		// g.setColor(Color.WHITE);
		// g.drawString(t, 4, 12);
		// }

		// draw a white border (should be close to field boundaries)
		//
		g.setColor(new Color(0xff, 0xe0, 0xe0, 0xe0));

		int mx = width / 2;
		int my = height / 2;
		int radius = 30;

		g.drawRect(border / 2, border / 2, width - border, height - border);
		g.drawLine(border / 2, height / 2, width - border / 2, height / 2);
		g.drawOval(mx - radius, my - radius, 2 * radius, 2 * radius);

		width -= border;
		height -= border;

		paintHits(g);
		paintShots(g);
		paintPlayers(g);

		// plot all position objects (players, ball, referee) that have
		// been added via the "update" method (see below)
		//
		for (int key = 0; key < positions.length; key++) {

			double[] pos = positions[key];
			if (pos == null || pos.length < 1 || pos[0] == 0.0)
				continue;

			// compute the coordinates (x|y) relative to the panel size
			//
			Double x = (pos[0] / HALF_WIDTH);
			x = border / 2 + (width * x);

			Double y = (pos[1] + HALF_HEIGHT) / (2 * HALF_HEIGHT);
			y = border / 2 + (height * y);

			// team A players have odd keys, team B players have even keys.
			// referee has key '0' and balls have keys < 0
			//
			//

			// int pid = SensorMapping.sid2pid[key];
			// if (pid < 0) {
			//
			// draw the ball in white, with a slightly smaller radius
			//
			g.setColor(Color.WHITE);
			g.fillOval(x.intValue() - 2, y.intValue() - 2, 4, 4);
			// }
		}
	}

	protected void paintPlayers(Graphics g) {
		for (int pid = 0; pid < players.length; pid++) {
			double[] pos = players[pid];
			if (pos == null) {
				log.info("No location for player {}", pid);
				continue;
			}

			if (pos[0] == 0.0) {
				continue;
			}

			Double x = (pos[0] / HALF_WIDTH);
			x = border / 2 + (width * x);

			Double y = (pos[1] + HALF_HEIGHT) / (2 * HALF_HEIGHT);
			y = border / 2 + (height * y);

			// pid == 0 is referee.
			// pid % 2 == 0 is team 'Blue'
			// pid % 2 == 1 is tream 'Red'
			//
			if (pid > 0) {
				if (pid % 2 == 0) {
					g.setColor(Color.BLUE);
				} else {
					g.setColor(Color.RED);
				}
			}

			if (pid == 0) {
				g.setColor(Color.BLACK);
			}

			Font f = g.getFont().deriveFont(8.0f);
			g.setFont(f);

			g.drawString("" + pid, x.intValue() + 4, y.intValue());

			// check if the player 'pid' is the one currently posessing
			// the ball
			//
			if (nearest >= 0 && nearest == pid) {
				g.setColor(Color.white);
				g.drawOval(x.intValue() - 6, y.intValue() - 6, 12, 12);
				if (pid > 0) {
					if (pid % 2 == 0)
						g.setColor(Color.cyan);
					else
						g.setColor(Color.PINK);

				}
			}
			g.fillOval(x.intValue() - 3, y.intValue() - 3, 6, 6);
		}
	}

	protected void paintHits(Graphics g) {
		for (double[] pos : hits) {
			Double x = (pos[0] / HALF_WIDTH);
			x = border / 2 + (width * x);

			Double y = (pos[1] + HALF_HEIGHT) / (2 * HALF_HEIGHT);
			y = border / 2 + (height * y);

			g.setColor(Color.CYAN);
			g.fillOval(x.intValue() - 2, y.intValue() - 2, 4, 4);
		}
	}

	protected void paintShots(Graphics g) {
		for (double[] pos : shots) {
			Double x = (pos[0] / HALF_WIDTH);
			x = border / 2 + (width * x);

			Double y = (pos[1] + HALF_HEIGHT) / (2 * HALF_HEIGHT);
			y = border / 2 + (height * y);

			g.setColor(Color.ORANGE);
			g.fillOval(x.intValue() - 2, y.intValue() - 2, 8, 8);
		}
	}

	public void setNearest(int pid) {
		nearest = pid;
	}

	public void updatePlayer(int pid, double x, double y) {
		players[pid] = new double[] { x, y };
	}

	public void addHit(int pid, double x, double y) {
		if (this.hits.size() > LAST_HITS) {
			hits.remove(0);
		}

		hits.add(new double[] { x, y });
		lastHit = pid;
	}

	public void addShot(int pid, double x, double y) {
		if (this.shots.size() > LAST_SHOTS) {
			shots.remove(0);
		}

		shots.add(new double[] { x, y });
		lastShot = pid;
	}

	public void updateBall(double x, double y) {
		positions[0] = new double[] { x, y };
	}

	public void update(Integer key, double x, double y, Integer leg) {

		if (count == 0) {
			firstUpdate = System.currentTimeMillis();
		}
		// update the position with the given key
		// and repaint the graphics
		//
		// log.info("Updating position for key {}", key);
		positions[key] = new double[] { x, y };
		count++;
		// if (count % 10 == 0) {
		repaint();
		validate();
		// }
	}
}