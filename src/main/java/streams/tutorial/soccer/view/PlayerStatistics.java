/**
 * 
 */
package streams.tutorial.soccer.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import streams.dashboard.Widget;

/**
 * @author chris
 * 
 */
public class PlayerStatistics extends Widget {

	/** The unique class ID */
	private static final long serialVersionUID = 7386037516189572070L;

	static Logger log = LoggerFactory.getLogger(PlayerStatistics.class);

	final PlayerTableModel tableModel = new PlayerTableModel();
	final JTable table = new JTable(tableModel);

	public PlayerStatistics() {
		setTitle("Player Statistics");
		tableModel.addColumn("total:distance", "total:distance");
		tableModel.addColumn("speed", "speed");
		tableModel.addColumn("x1", "x1");
		tableModel.addColumn("x2", "x2");
		tableModel.addColumn("x3", "x3");

		tableModel.addPlayer(1, "Nick Gertje");
		tableModel.addPlayer(3, "Dennis Dotterweich");
		tableModel.addPlayer(5, "Niklas Waelzlein");
		tableModel.addPlayer(7, "Willi Sommer");
		tableModel.addPlayer(9, "Philipp Harlass");
		tableModel.addPlayer(11, "Roman Hartleb");
		tableModel.addPlayer(13, "Erik Engelhardt");
		tableModel.addPlayer(15, "Sandro Schneider");
		tableModel.addPlayer(2, "Leon Krapf");
		tableModel.addPlayer(4, "Kevin Baer");
		tableModel.addPlayer(6, "Luca Ziegler");
		tableModel.addPlayer(8, "Ben Mueller");
		tableModel.addPlayer(10, "Vale Reitstetter");
		tableModel.addPlayer(12, "Christopher Lee");
		tableModel.addPlayer(14, "Leon Heinze");
		tableModel.addPlayer(16, "Leo Langhans");

		table.getTableHeader().setDefaultRenderer(
				new PlayerStatisticsTableHeaderRenderer());

		table.setRowHeight(32);
		table.setBorder(null);
		JScrollPane scroll = new JScrollPane(table);
		scroll.setBorder(null);
		this.setContent(scroll);
	}

	/**
	 * @see streams.dashboard.Widget#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		if (pid != null && pid > 0) {

			for (String col : tableModel.columns) {
				if ("name".equals(col)) {
					continue;
				}

				if (input.containsKey(col)) {
					// log.info("updating {} of player {} ", col, pid);
					this.tableModel.set(pid, col, input.get(col));
				}
			}
		}

		return input;
	}

	public static class PlayerTableModel extends AbstractTableModel {

		private static final long serialVersionUID = -6109509762291459560L;
		final Map<Integer, Map<String, Object>> tableData = new LinkedHashMap<Integer, Map<String, Object>>();

		final ArrayList<String> columns = new ArrayList<String>();
		final LinkedHashMap<String, String> columnNames = new LinkedHashMap<String, String>();

		public PlayerTableModel() {
			addColumn("pid", "pid");
			addColumn("name", "Name");
			this.fireTableDataChanged();
		}

		public void addColumn(String key, String name) {
			this.columns.add(key);
			this.columnNames.put(key, name);
			this.fireTableStructureChanged();
		}

		public void addPlayer(Integer pid, String name) {
			Map<String, Object> row = new LinkedHashMap<String, Object>();
			row.put("pid", pid);
			row.put("name", name);
			tableData.put(pid, row);
			this.fireTableDataChanged();
		}

		public void set(Integer pid, String key, Object value) {
			Map<String, Object> row = this.tableData.get(pid);
			if (row != null) {
				row.put(key, value);
				this.fireTableDataChanged();
			}
		}

		/**
		 * @see javax.swing.table.DefaultTableModel#getColumnName(int)
		 */
		@Override
		public String getColumnName(int column) {
			return columns.get(column);
		}

		/**
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		@Override
		public int getRowCount() {
			return tableData.size();
		}

		/**
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		@Override
		public int getColumnCount() {
			return columns.size();
		}

		/**
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {

			List<Integer> keys = new ArrayList<Integer>(tableData.keySet());

			Integer pid = keys.get(rowIndex);
			if (pid != null) {
				Map<String, Object> row = tableData.get(pid);

				String col = columns.get(columnIndex);
				return row.get(col);
			} else {
				return "";
			}
		}
	}
}
