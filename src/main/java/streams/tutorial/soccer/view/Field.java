/**
 * 
 */
package streams.tutorial.soccer.view;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import streams.dashboard.Widget;

/**
 * <p>
 * A simple widget that provides a display for the DEBS soccer challenge data.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class Field extends Widget {

	/** The unique class ID */
	private static final long serialVersionUID = -5144865662831561406L;
	static Logger log = LoggerFactory.getLogger(Field.class);

	final SoccerFieldPanel fieldPanel = new SoccerFieldPanel();

	final SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss,S");

	Long updates = 0L;

	public Field() {
		this.setContent(fieldPanel);
		this.setTitle("\"Live\" View");
	}

	/**
	 * @see streams.dashboard.Widget#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");

		Double playerX = (Double) input.get("player.x");
		Double playerY = (Double) input.get("player.y");

		if (pid >= 0 && playerX != null && playerY != null) {
			// fieldPanel.update(pid, playerX, playerY, 1);
			// log.info("Updating player {} at {}", pid, playerX + "," +
			// playerY);
			fieldPanel.updatePlayer(pid, playerX, playerY);
			// fieldPanel.repaint();
		}

		if (pid < 0) {
			Double posX = (Double) input.get("pos.x");
			Double posY = (Double) input.get("pos.y");
			if (posX != null && posY != null) {
				fieldPanel.updateBall(posX, posY);
			}
		}

		updates++;
		if (updates.longValue() % 25 == 0L) {
			// Long timestamp = (Long) input.get("timestamp");
			// Date date = new Date(timestamp / 1000L / 1000L / 1000L);
			// fieldPanel.updateTime(fmt.format(date));

			fieldPanel.repaint();
		}

		return input;
	}
}
