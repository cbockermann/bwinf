/**
 * 
 */
package streams.tutorial.soccer;

import java.util.HashMap;
import java.util.Map;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class SimpleAverage implements Processor {

	Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
	Map<Integer, Double> sum = new HashMap<Integer, Double>();

	String key = "distance";
	Integer every = 200;

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");

		Double value = (Double) input.get(key);
		if (value != null) {

			Integer count = counts.get(pid);
			if (count == null) {
				count = 1;
			}

			Double s = sum.get(pid);
			if (s == null) {
				s = 0.0;
			}

			s += value;

			if (count % every == 0) {
				input.put("avg:" + key, s / every.doubleValue());
				System.out.println("adding avg for " + pid + " = "
						+ (s / every.doubleValue()));
				s = 0.0;
			}

			sum.put(pid, s);
			counts.put(pid, count + 1);
		}

		return input;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the every
	 */
	public Integer getEvery() {
		return every;
	}

	/**
	 * @param every
	 *            the every to set
	 */
	public void setEvery(Integer every) {
		this.every = every;
	}

}
