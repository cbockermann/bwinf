/**
 * 
 */
package streams.tutorial.soccer;

import stream.Data;
import stream.Processor;
import stream.data.DataFactory;
import stream.io.Sink;

/**
 * <p>
 * A simple aggregation of the running distance.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class Aggregate implements Processor {

	Double agg = 0.0;
	Sink output;

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		if (pid < 1) {
			return input;
		}

		Double value = (Double) input.get("distance");

		if (value != null) {
			agg += value;
		}

		if (output != null) {
			Data out = DataFactory.create();
			out.put("total:distance", agg);
			input.put("total:distance", agg);
			try {
				output.write(out);
			} catch (Exception e) {
			}
		}

		return input;
	}

	/**
	 * @return the output
	 */
	public Sink getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(Sink output) {
		this.output = output;
	}
}
