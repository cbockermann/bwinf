/**
 * 
 */
package streams.tutorial.soccer;

import java.awt.Color;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import stream.io.SourceURL;

/**
 * <p>
 * A simple dashboard for displaying soccer data.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class Dashboard extends streams.dashboard.Dashboard {

	/** The unique class ID */
	private static final long serialVersionUID = 7318699843486630008L;

	static Logger log = LoggerFactory.getLogger(Dashboard.class);

	public Dashboard() {
		super();
		// this.setBackground(new Color(21, 30, 3));
		// this.content.setBackground(new Color(21, 30, 3));
		// this.display.setBackground(new Color(21, 30, 3));

		String bgKey = this.getClass().getCanonicalName() + ".background";
		log.debug("background color key is '{}'", bgKey);

		this.setBackground(colors.get(bgKey, Color.WHITE));
		display.setBackground(colors.get(bgKey, Color.WHITE));
		display.setBackground(colors.get(bgKey, Color.WHITE));

		try {
			this.setConfig(new SourceURL("classpath:/soccer/dashboard.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.setSize(1024, 768);
	}

	/**
	 * @see stream.Configurable#configure(org.w3c.dom.Element)
	 */
	public void configure(Element document) {
	}
}
