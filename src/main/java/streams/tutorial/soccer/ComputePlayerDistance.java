/**
 * 
 */
package streams.tutorial.soccer;

import stream.Data;
import stream.Processor;

/**
 * <p>
 * This processor computes the distance between the last position of a player
 * and her current one.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class ComputePlayerDistance implements Processor {

	Double lastX[] = new Double[24];
	Double lastY[] = new Double[24];

	public ComputePlayerDistance() {
		for (int i = 0; i < lastX.length; i++) {
			lastX[i] = null;
			lastY[i] = null;
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data event) {

		Integer pid = (Integer) event.get("pid");
		if (pid < 1) {
			// skip all sensor readings with no valid player ID (e.g. ball
			// sensor, referee)
			return event;
		}

		Double currentX = (Double) event.get("player.x");
		Double currentY = (Double) event.get("player.y");
		if (currentX == null) {
			//
			// event does not provide any player positions, so we simply
			// skip this event
			//
			return event;
		}

		if (lastX[pid] != null || lastY[pid] != null) {
			//
			// compute the euclidean distance and add it as field 'distance'
			// to the event
			//
			double dist = distance(lastX[pid], lastY[pid], currentX, currentY);
			event.put("distance", dist);
		}

		lastX[pid] = currentX;
		lastY[pid] = currentY;
		return event;
	}

	/**
	 * Simple implementation of euclidean distance
	 * 
	 * @param x
	 * @param y
	 * @param cx
	 * @param cy
	 * @return
	 */
	public double distance(double x, double y, double cx, double cy) {
		double dx = (x - cx);
		double dy = (y - cy);
		return Math.sqrt(dx * dx + dy * dy);
	}
}
