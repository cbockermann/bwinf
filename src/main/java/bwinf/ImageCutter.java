package bwinf;

import stream.Data;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

public class ImageCutter extends AbstractImageProcessor 
{
	private int offsetX = 0, offsetY = 0, width = 0, height = 0;
	
	public int getOffsetX() {
		return offsetX;
	}

	public void setOffsetX(int offsetX) {
		this.offsetX = offsetX;
	}

	public int getOffsetY() {
		return offsetY;
	}

	public void setOffsetY(int offsetY) {
		this.offsetY = offsetY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public Data process(Data item, ImageRGB image) 
	{
		int[] oldPixels = image.getPixels();
		int[] areaPixels = new int[oldPixels.length];
		for(int i = 0; i < areaPixels.length; i++)
		{
			areaPixels[i] = oldPixels[i];
		}
		
		
		int oldHeight = image.getHeight();
		int oldWidth = image.getWidth();
		
		int [] pixels = new int[height * width];
		
		
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				pixels[y * width + x] = oldPixels[(y + offsetY) * oldWidth + x + offsetX];
				areaPixels[(y + offsetY) * oldWidth + x + offsetX] = 0;
			}
		}
		
		ImageRGB newImage = new ImageRGB(width, height, pixels);
		ImageRGB areaImage = new ImageRGB(oldWidth, oldHeight, oldPixels);
		
		item.put("cuttedImage", newImage);
		item.put("areaImage", areaImage);
		
		return item;
	}
}
