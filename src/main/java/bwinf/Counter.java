package bwinf;

import stream.Data;
import stream.Processor;

public class Counter implements Processor 
{
	int gesamt = 0, blue, green, brown, gold, red, purple;
	
	
	@Override
	public Data process(Data item) 
	{
		gesamt++;
		
		if((Color)item.get("color") == Color.blue)
		{
			blue++;
		}
		else if((Color)item.get("color") == Color.green)
		{
			green++;
		}
		else if((Color)item.get("color") == Color.brown)
		{
			brown++;
		}
		else if((Color)item.get("color") == Color.gold)
		{
			gold++;
		}
		else if((Color)item.get("color") == Color.red)
		{
			red++;
		}
		else if((Color)item.get("color") == Color.purple)
		{
			purple++;
		}
		
		item.put("gesamt", gesamt);
		item.put("gesamt:blue", blue);
		item.put("gesamt:green", green);
		item.put("gesamt:brown", brown);
		item.put("gesamt:gold", gold);
		item.put("gesamt:red", red);
		item.put("gesamt:purple", purple);
		
		System.out.println("Gesamt" + gesamt);
		System.out.println("Rot" + red);
		System.out.println("Gold" + gold);
		System.out.println("---------------------");
		

		return item;
	}

}
