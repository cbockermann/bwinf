package bwinf;

/**
 * 
 */

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class Start {

    static Logger log = LoggerFactory.getLogger(Start.class);

    public static void main(String[] args) throws Exception {
        URL url = Start.class.getResource("/config/bwinf.xml");
        log.info("Starte BWINF Test Projekt von {}", url);
        stream.run.main(url);
    }
}
