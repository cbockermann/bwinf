package bwinf;

import stream.Data;
import stream.Processor;
import stream.image.ImageRGB;

public class CapsuleWatcher implements Processor
{
	private double threshold;
	private boolean capsule = false;
	private ImageRGB lastImage;
	
	public double getThreshold() 
	{
		return threshold;
	}
	public void setThreshold(double threshold) 
	{
		this.threshold = threshold;
	}
	
	public Data process(Data item) 
	{
		double diffminRGB = (Double)item.get("diffminRGB");
		
		item.put("capsule", (double) 0.0);
		
		if(capsule == false && diffminRGB < threshold)
		{
			capsule = true;
		}
		
		if(capsule == true && diffminRGB >= 0)
		{
			capsule = false;
			item.put("capsuleImage", lastImage);			
		}
		
		lastImage = (ImageRGB)item.get("cuttedImage");
		
		return item;
	}
}
