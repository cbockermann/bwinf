package bwinf;

import java.util.Map;

import stream.Data;
import stream.Processor;

public class ColorChoser implements Processor {

	@Override
	public Data process(Data item) 
	{
		double red = (Double)item.get("avg:red");
		double green = (Double)item.get("avg:green");
		double blue = (Double)item.get("avg:blue");
		
		double[] capsuleGold = {85, 75, 52};
		double[] capsuleRed = {75, 55, 52};
		
		double goldDiff = Math.abs(capsuleGold[0] - red) + Math.abs(capsuleGold[1] - green) + Math.abs(capsuleGold[2] - blue);
		double redDiff = Math.abs(capsuleRed[0] - red) + Math.abs(capsuleRed[1] - green) + Math.abs(capsuleRed[2] - blue);
		
		if(redDiff < goldDiff)
		{
			item.put("color", Color.red);
			System.out.println("rot");
		}
		else
		{
			item.put("color", Color.gold);
			System.out.println("gold");
		}
		
		return item;
	}

}
