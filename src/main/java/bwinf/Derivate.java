package bwinf;

import stream.Data;
import stream.Processor;

public class Derivate  implements Processor 
{
	private String key;
	double vergangen;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Data process(Data item) 
	{
		double aktuell = (Double)item.get(key);
		double diff = aktuell - vergangen;
		vergangen = aktuell;
		item.put( "diff" + key, diff);
		return item;
	}
}
		

