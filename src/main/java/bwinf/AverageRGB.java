package bwinf;

import stream.Data;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

public class AverageRGB extends AbstractImageProcessor {

    /**
     * 
     * Beispiel-Prozessor zur Berechnung der durchschnittlichen Rot/Gruen/Blau
     * Werte ueber allen Pixeln eines Bildes.
     * 
     */
    @Override
    public Data process(Data item, ImageRGB image) {

        int[] pixels = image.getPixels();
        double numberOfPixels = pixels.length;
        double summeRot = 0.0;
        double summeGruen = 0.0;
        double summeBlau = 0.0;

        for (int i = 0; i < pixels.length; i++) {

            // Rot-Anteil des Pixels bestimmen und zur Rot-Summe addieren
            //
            int r = (0x00ff0000 & pixels[i]) >> 16;
            summeRot += r;

            // Gruen-Anteil des Pixels bestimmen und zur Gruen-Summe addieren
            //
            int g = (0x0000ff00 & pixels[i]) >> 8;
            summeGruen += g;

            // Blau-Anteil des Pixels bestimmen und zur Blau-Summe addieren
            //
            int b = (0x000000ff & pixels[i]);
            summeBlau += b;
        }

        double avgRot = summeRot / numberOfPixels;
        double avgGruen = summeGruen / numberOfPixels;
        double avgBlau = summeBlau / numberOfPixels;

        item.put("avg:red", avgRot);
        item.put("avg:green", avgGruen);
        item.put("avg:blue", avgBlau);

        return item;
    }
}
