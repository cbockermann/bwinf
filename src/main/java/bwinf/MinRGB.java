package bwinf;

import stream.Data;
import stream.Processor;

public class MinRGB implements Processor 
{
	@Override
	public Data process(Data item) 
	{
		double min = (Double)item.get("avg:red");
		if((Double)item.get("avg:green") < min)
		{
			min = (Double)item.get("avg:green");
		}
		if((Double)item.get("avg:green") < min)
		{
			min = (Double)item.get("avg:green");
		}
		
		
		item.put("minRGB", min);
		
		return item;
	}

}
